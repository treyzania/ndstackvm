#![allow(unused)]

use std::collections::*;
use std::mem;
use std::rc::Rc;

use isn;

#[derive(Clone)]
enum Value {
    Num(i64),
    Buf(Vec<u8>),
    SubVm(Box<Vm>),
}

#[derive(Clone)]
struct StackElement {
    val: Value,
    prev: Option<Rc<StackElement>>
}

impl StackElement {
    fn destructure(self) -> (Value, Option<Rc<StackElement>>) {
        (self.val, self.prev)
    }

    fn peek_at(&self, i: u64) -> Option<&Value> {
        match (self.prev.as_ref(), i) {
            (_, 0) => Some(&self.val),
            (Some(v), i) => v.peek_at(i - 1),
            (None, i) => None
        }
    }
}

#[derive(Clone)]
struct Stack {
    head: Option<Rc<StackElement>>
}

impl Stack {
    fn empty() -> Stack {
        Stack {
            head: None
        }
    }

    fn push(&mut self, val: Value) {
        // Copy the prev.
        let mut prev = self.head.take();

        // Construct the new frame.
        let frame = StackElement {val, prev};

        // Update the head with the new frame.
        self.head = Some(Rc::new(frame));
    }

    fn pop(&mut self) -> Option<Value> {
        let head = self.head.take();
        if head.is_some() {
            // Do some weird logic to get the frame itself.
            let (v, p) = head
                .unwrap()
                .as_ref()
                .clone()
                .destructure();

            // Take the inner stuff as our own.
            self.head = p;
            Some(v)
        } else {
            None
        }
    }

    /// Pops the first n items from the stack, returns false if there's an underflow.
    fn drop_n(&mut self, n: u32) -> bool {
        for i in 0..n {
            let popped = self.pop();
            if popped.is_none() {
                return false;
            }
        }
        return true;
    }

    fn peek(&self) -> Option<&Value> {
        self.peek_at(0)
    }

    fn peek_at(&self, i: u64) -> Option<&Value> {
        match self.head {
            Some(ref v) => v.peek_at(i),
            None => None
        }
    }
}

#[derive(Clone)]
pub struct Machine {
    stack: Stack,
    alt_stack: Stack,

    isn_pointer: usize
}

enum ExecSegue {
    Normal,
    Branch(usize),
    Reject,
    Accept
}

enum IsnError {
    StackUnderflow,
    AltStackUnderflow,
    ValueMismatch,
    InvalidJump,
    Unimplemented,
    IndexOutOfBounds,
}

fn exec_isn(
    mach: &mut Machine,
    isn: &isn::Instruction
) ->
    Result<ExecSegue, IsnError>
{
    use self::ExecSegue::*;
    use isn::Instruction::*;

    // TODO Remove some of these unnecessary clones.  It has to do with the way
    // we return refs from the stack.
    match isn {

        Nop => {},

        PushNum(v) => {
            mach.stack.push(Value::Num(*v));
        },

        PushIsnPtr => {
            mach.stack.push(Value::Num(mach.isn_pointer as i64));
        },

        PushBuf(buf) => {
            mach.stack.push(Value::Buf(Vec::from(buf.clone())));
        }

        Dup => match mach.stack.peek().cloned() {
            Some(v) => {
                mach.stack.push(v.clone());
            },
            None => return Err(IsnError::StackUnderflow)
        },

        DupUnder => match mach.stack.peek_at(1).cloned() {
            Some(v) => {
                mach.stack.push(v.clone());
            },
            None => return Err(IsnError::StackUnderflow)
        }

        Pop => {
            match mach.stack.pop() {
                Some(_) => {},
                None => return Err(IsnError::StackUnderflow)
            }
        }

        Add => {
            let a = mach.stack.peek_at(0).cloned();
            let b = mach.stack.peek_at(1).cloned();
            match (a, b) {
                (Some(Value::Num(av)), Some(Value::Num(bv))) => {
                    mach.stack.drop_n(2);
                    mach.stack.push(Value::Num(av + bv));
                },
                (Some(_), Some(_)) => {
                    return Err(IsnError::ValueMismatch);
                },
                (_, _) => {
                    return Err(IsnError::StackUnderflow);
                }
            }
        },

        Sub => {
            let a = mach.stack.peek_at(0).cloned();
            let b = mach.stack.peek_at(1).cloned();
            match (a, b) {
                (Some(Value::Num(av)), Some(Value::Num(bv))) => {
                    mach.stack.drop_n(2);
                    mach.stack.push(Value::Num(bv - av));
                },
                (Some(_), Some(_)) => {
                    return Err(IsnError::ValueMismatch);
                },
                (_, _) => {
                    return Err(IsnError::StackUnderflow);
                }
            }
        },

        Fork(off) => {
            let forked_ip = (mach.isn_pointer as isize + off) as usize;
            mach.isn_pointer += 1;
            return Ok(ExecSegue::Branch(forked_ip));
        }

        Die => return Ok(Reject),

        Finish => return Ok(Accept),

        JumpAbs(addr) => {
            mach.isn_pointer = *addr;
            return Ok(ExecSegue::Normal);
        },

        JumpRel(off) => {
            mach.isn_pointer = (mach.isn_pointer as isize + off) as usize;
            return Ok(ExecSegue::Normal);
        },

        Jump => match mach.stack.pop() {
            Some(Value::Num(v)) => {
                mach.isn_pointer = v as usize;
                return Ok(ExecSegue::Normal);
            },
            Some(_) => return Err(IsnError::ValueMismatch),
            None => return Err(IsnError::StackUnderflow)
        },

        ToAlt => match mach.stack.pop() {
            Some(v) => {
                mach.alt_stack.push(v);
            },
            None => return Err(IsnError::StackUnderflow)
        },

        FromAlt => match mach.alt_stack.pop() {
            Some(v) => {
                mach.stack.push(v);
            },
            None => return Err(IsnError::AltStackUnderflow)
        },

        Swap2 => {
            let a = mach.stack.peek_at(0).cloned();
            let b = mach.stack.peek_at(1).cloned();
            match (a, b) {
                (Some(av), Some(bv)) => {
                    mach.stack.drop_n(2);
                    mach.stack.push(bv.clone());
                    mach.stack.push(av.clone());
                },
                (Some(_), Some(_)) => {
                    return Err(IsnError::ValueMismatch);
                },
                (_, _) => {
                    return Err(IsnError::StackUnderflow);
                }
            }
        },

        BufLen => {
            let top_len = mach.stack.peek()
                .map(|v| match v {
                    Value::Buf(b) => Some(b.len()),
                    _ => None
                });

            match top_len {
                Some(Some(l)) => mach.stack.push(Value::Num(l as i64)),
                Some(None) => return Err(IsnError::ValueMismatch),
                None => return Err(IsnError::StackUnderflow),
            }
        },

        BufAppend => {
            let a = mach.stack.pop();
            let b = mach.stack.pop();
            match (a, b) {
                (Some(Value::Buf(a)), Some(Value::Buf(mut b))) => {
                    b.extend(a);
                    mach.stack.push(Value::Buf(b));
                },
                (Some(a), Some(b)) => {
                    mach.stack.push(b);
                    mach.stack.push(a);
                    return Err(IsnError::ValueMismatch)
                },
                (Some(a), _) => {
                    mach.stack.push(a);
                    return Err(IsnError::StackUnderflow);
                },
                _ => return Err(IsnError::StackUnderflow)
            }
        },

        BufIndex => {
            // Figure out what index we're looking for.
            let index_val = mach.stack.pop();
            let i = match index_val {
                Some(Value::Num(n)) => n,
                Some(a) => {
                    mach.stack.push(a);
                    return Err(IsnError::ValueMismatch);
                },
                _ => return Err(IsnError::StackUnderflow)
            };

            // Figure out what it is we're looking at in the buf.
            let v = match mach.stack.peek() {
                Some(Value::Buf(b)) => {
                    if i < 0 || b.len() >= i as usize {
                        None
                    } else {
                        Some(b[i as usize] as i64)
                    }
                },
                Some(a) => return Err(IsnError::ValueMismatch),
                None => return Err(IsnError::StackUnderflow)
            };

            // Actually push the value possibly.
            match v {
                Some(val) => mach.stack.push(Value::Num(val as i64)),
                None => return Err(IsnError::IndexOutOfBounds)
            }
        },

        VmInit => {
            // First we have to figure out how to parse the data.
            let isns = match mach.stack.peek() {
                Some(Value::Buf(buf)) => match isn::sequence_from(buf) {
                    Ok(parsed) => parsed,
                    Err(_) => return Err(IsnError::ValueMismatch),
                },
                Some(_) => return Err(IsnError::ValueMismatch),
                None => return Err(IsnError::StackUnderflow)
            };

            // Then we replace the top element with the new VM.
            mach.stack.pop();
            mach.stack.push(Value::SubVm(Box::new(Vm::new(isns))));
        },

        VmStep => {
            match mach.stack.pop() {
                Some(Value::SubVm(mut vm)) => {
                    vm.exec_once();
                    mach.stack.push(Value::SubVm(vm));
                },
                Some(v) => {
                    mach.stack.push(v);
                    return Err(IsnError::ValueMismatch);
                },
                None => return Err(IsnError::StackUnderflow)
            }
        }

        _ => return Err(IsnError::Unimplemented)

    }

    // If we get to this point then just do the normal stuff.
    mach.isn_pointer += 1;
    Ok(Normal)
}

#[derive(Clone)]
pub struct Vm {
    program: Vec<isn::Instruction>,
    machines: Vec<Machine>
}

impl Vm {
    pub fn new(prog: Vec<isn::Instruction>) -> Vm {
        Vm {
            program: prog,
            machines: vec![Machine {
                stack: Stack::empty(),
                alt_stack: Stack::empty(),
                isn_pointer: 0,
            }]
        }
    }

    pub fn exec_once(&mut self) {
        let mut to_remove = Vec::new();
        let mut to_add = Vec::new();

        // Loop through all the machines and figure out what to do.
        for mi in 0..self.machines.len() {
            let mut m = &mut self.machines[mi];
            if m.isn_pointer >= self.program.len() {
                to_remove.push(mi)
            }

            let mip = m.isn_pointer;

            use self::ExecSegue::*;
            match exec_isn(&mut m, &self.program[mip]) {
                Ok(Normal) => {},
                Ok(Branch(br_addr)) => {
                    let mut dup = m.clone();
                    dup.isn_pointer = br_addr;
                    to_add.push(dup);
                },
                Ok(Reject) => {
                    to_remove.push(mi);
                },
                Ok(Accept) => {
                    // TODO
                    println!("machine accepted");
                }
                Err(e) => {
                    // Idk what else to do.
                    to_remove.push(mi);
                }
            }
        }

        // Remove old machines.
        for i in 0..to_remove.len() {
            let mi = to_remove[i];
            self.machines.remove(mi - i); // weird index because ordering
        }

        // Now add on the new machines.
        self.machines.extend(to_add);
    }

    pub fn exec_n_isns(&mut self, n: u32) {
        for i in 0..n {
            self.exec_once();
        }
    }
}
