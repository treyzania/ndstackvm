extern crate byteorder;
#[macro_use] extern crate clap;

use std::fs;
use std::io::{BufReader, BufWriter};
use std::io::prelude::*;
use std::num;
use std::path;

mod isn;
mod vm;

use isn::Instruction::{self, *};

fn main() {
    let matches =
        clap_app!(ndpdac =>
                  (version: "1.0")
                  (author: "Trey Del Bonis")
                  (about: "NdStackVM bytecode compiler")
                  (@arg program: +required "source")
                  (@arg output: -o --output +takes_value "output")).get_matches();

    let src_path = matches.value_of("program").unwrap();
    let dest_path = matches.value_of("output").unwrap_or("out.bin");
    
    let fr = {
        let p = path::PathBuf::from(src_path);
        let f = fs::File::open(p).expect("open");
        BufReader::new(f)
    };

    let isns = match parse_text(fr) {
        Ok(isns) => isns,
        Err(e) => {
            eprintln!("parse error: {:?}", e);
            return;
        }
    };

    let mut fw = {
        let p = path::PathBuf::from(dest_path);
        let f = fs::File::create(p).expect("open");
        BufWriter::new(f)
    };

    isn::serialize(isns, &mut fw).expect("write");

}

#[derive(Debug)]
enum Error {
    UnknownPattern,
    TooManyArgs,
    NumParseError
}

impl From<num::ParseIntError> for Error {
    fn from(_: num::ParseIntError) -> Self {
        Error::NumParseError
    }
}

fn parse_text(data: impl BufRead) -> Result<Vec<Instruction>, Error> {
    Ok(data.lines()
       .map(|v| v.unwrap()
            .trim()
            .split(" ")
            .map(String::from)
            .collect())
       .filter(|v: &Vec<_>| v.len() > 0 && !v.first().unwrap().is_empty())
       .map(parse_isn)
       .map(Result::unwrap) // bad
       .collect())
}

fn parse_isn(stmt: Vec<String>) -> Result<Instruction, Error> {

    if stmt.len() > 2 {
        return Err(Error::TooManyArgs);
    }

    let isn: Option<&str> = stmt.iter().nth(0).map(|s| s.as_ref());
    let arg: Option<&str> = stmt.iter().nth(1).map(|s| s.as_ref());
    
    match (isn, arg) {
        (Some("nop"), None) => Ok(Nop),
        (Some("push"), Some(a)) => {
            let v: i64 = a.parse()?;
            Ok(PushNum(v))
        },
        (Some("puship"), None) => Ok(PushIsnPtr),
        (Some("dup"), None) => Ok(Dup),
        (Some("dupu"), None) => Ok(DupUnder),
        (Some("pop"), None) => Ok(Pop),
        (Some("add"), None) => Ok(Add),
        (Some("sub"), None) => Ok(Sub),
        (Some("fork"), Some(a)) => {
            let v: u64 = a.parse()?;
            Ok(Fork(v as isize))
        },
        (Some("die"), None) => Ok(Die),
        (Some("fin"), None) => Ok(Finish),
        (Some("jumpa"), Some(a)) => {
            let v: u64 = a.parse()?;
            Ok(JumpAbs(v as usize))
        },
        (Some("jumpr"), Some(a)) => {
            let v: i64 = a.parse()?;
            Ok(JumpRel(v as isize))
        },
        (Some("jump"), None) => Ok(Jump),
        (Some("toalt"), None) => Ok(ToAlt),
        (Some("fromalt"), None) => Ok(FromAlt),
        (Some("swap2"), None) => Ok(Swap2),
        (n, m) => {
            eprintln!("unknown: {:?} {:?}", n, m);
            Err(Error::UnknownPattern)
        } // I hope
    }
}
