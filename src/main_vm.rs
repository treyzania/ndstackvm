extern crate byteorder;
#[macro_use] extern crate clap;

use std::fs;
use std::io::prelude::*;
use std::path;

mod isn;
mod vm;

use isn::Error;
use vm::Vm;

fn main() {
    let matches =
        clap_app!(ndpdavm =>
                  (version: "1.0")
                  (author: "Trey Del Bonis")
                  (about: "nondeterministic stack machine VM interpreter")
                  (@arg program: +required "bytecode binary")).get_matches();

    let prog_path = matches.value_of("program").unwrap();

    let bytecode = {        
        let path = path::PathBuf::from(prog_path);
        let mut f = fs::File::open(path).expect("open");
        let mut buf = Vec::new();
        f.read_to_end(&mut buf).expect("read");
        buf
    };

    let isns = match isn::sequence_from(bytecode.as_ref()) {
        Ok(v) => v,
        Err(Error::InvalidOp(off)) => {
            eprintln!("parse error: invalid instruction at {}", off);
            return;
        },
        Err(e) => {
            eprintln!("parse error: {:?}", e);
            return;
        }
    };

    println!("{:?}", isns);
    
    let mut vm = Vm::new(isns);

    loop {
        vm.exec_once();
    }
        
}
