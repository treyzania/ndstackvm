#![allow(unused)]

use std::io::{self, Read, Write};

use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub enum Instruction {

    /// Do nothing.
    Nop,

    /// Push a number.
    PushNum(i64),

    /// Push the instruction pointer onto the stack.
    PushIsnPtr,

    /// Push a buffer onto the stack.
    PushBuf(Box<[u8]>),

    /// Duplicate the top element, pushing it onto the stack.
    Dup,

    /// Duplicate the element under the top, pushing it onto the stack.
    DupUnder,

    /// Pop the top of the stack, forgetting about it.
    Pop,

    /// Pop the top 2 elements and push the sum.
    Add,

    /// Pop the top 2 elements and push the difference.
    Sub,

    /// Fork a new submachine with a given instruction offset.
    Fork(isize),

    /// Kill this machine.
    Die,

    /// Kills all other machines (if there are any) and yields the top of the
    /// stack as a result.
    Finish,

    /// Jump to a new address.
    JumpAbs(usize),

    /// Add this value to the instruction pointer.
    JumpRel(isize),

    /// Pop the address on top of the stack and set the ins pointer to it.
    Jump,

    /// Pop the top element and push it to the alt stack.
    ToAlt,

    /// Pop the top element of the alt stack and push it to the main stack.
    FromAlt,

    /// Swap the top 2 elements.
    Swap2,

    /// Push the length of the buffer at the top of the stack.
    BufLen,

    /// Pop the top 2 buffer of the stack, appends them, pushes it.
    BufAppend,

    /// Pop an index, push the value in the buffer at that index.
    BufIndex,

    /// Converts the number at the top of the stack into a buffer, assuming <255.
    NumToBuf,

    /// Pops the buffer at the top of the stack and uses it as the program for a
    /// new VM, which is pushed onto the stack.
    VmInit,

    /// Pops the VM at the top of the stack, runs one step, and pushes the VM
    /// back onto the stack.
    /// TODO Figure out IO for this VM.
    VmStep

}

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    InvalidOp(u64)
}

impl From<io::Error> for Error {
    fn from(f: io::Error) -> Self {
        Error::Io(f)
    }
}

pub fn sequence_from(buf: &[u8]) -> Result<Vec<Instruction>, Error> {
    use self::Instruction::*;

    let mut isns = Vec::new();
    let mut cur = io::Cursor::new(buf);
    while cur.position() < buf.len() as u64 {
        let isn = match cur.read_u8()? {
            0x00 => Nop,
            0x10 => PushNum(cur.read_i64::<BigEndian>()?),
            0x11 => PushIsnPtr,
            0x12 => {
                let len = cur.read_u32::<BigEndian>()? as usize;
                let mut buf = vec![0; len];
                cur.read_exact(buf.as_mut_slice())?;
                PushBuf(buf.into_boxed_slice())
            },
            0x13 => Dup,
            0x14 => DupUnder,
            0x15 => Pop,
            0x20 => Add,
            0x21 => Sub,
            0x30 => Fork(cur.read_i64::<BigEndian>()? as isize),
            0x31 => Die,
            0x32 => Finish,
            0x40 => JumpAbs(cur.read_u64::<BigEndian>()? as usize),
            0x41 => JumpRel(cur.read_i64::<BigEndian>()? as isize),
            0x42 => Jump,
            0x50 => ToAlt,
            0x51 => FromAlt,
            0x52 => Swap2,
            0x60 => BufLen,
            0x61 => BufAppend,
            0x62 => BufIndex,
            0x63 => NumToBuf,
            0xf0 => VmInit,
            0xf1 => VmStep,
            _ => return Err(Error::InvalidOp(cur.position()))
        };
        isns.push(isn);
    }

    Ok(isns)
}

pub fn serialize<W: WriteBytesExt>(prog: Vec<Instruction>, out: &mut W) -> Result<(), Error> {
    use self::Instruction::*;

    for i in prog {
        match i {
            Nop => out.write_u8(0x00)?,
            PushNum(n) => {
                out.write_u8(0x10)?;
                out.write_i64::<BigEndian>(n)?;
            },
            PushIsnPtr => out.write_u8(0x11)?,
            PushBuf(buf) => {
                out.write_u8(0x12)?;
                out.write_u32::<BigEndian>(buf.len() as u32)?;
                out.write_all(&buf)?;
            }
            Dup => out.write_u8(0x13)?,
            DupUnder => out.write_u8(0x14)?,
            Pop => out.write_u8(0x15)?,
            Add => out.write_u8(0x20)?,
            Sub => out.write_u8(0x21)?,
            Fork(off) => {
                out.write_u8(0x30)?;
                out.write_i64::<BigEndian>(off as i64)?;
            },
            Die => out.write_u8(0x31)?,
            Finish => out.write_u8(0x32)?,
            JumpAbs(addr) => {
                out.write_u8(0x40)?;
                out.write_u64::<BigEndian>(addr as u64)?;
            },
            JumpRel(off) => {
                out.write_u8(0x41)?;
                out.write_i64::<BigEndian>(off as i64)?;
            },
            Jump => out.write_u8(0x42)?,
            ToAlt => out.write_u8(0x50)?,
            FromAlt => out.write_u8(0x51)?,
            Swap2 => out.write_u8(0x52)?,
            BufLen => out.write_u8(0x60)?,
            BufAppend => out.write_u8(0x61)?,
            BufIndex => out.write_u8(0x62)?,
            NumToBuf => out.write_u8(0x63)?,
            VmInit => out.write_u8(0xf0)?,
            VmStep => out.write_u8(0xf1)?,
        }
    }

    Ok(())
}
